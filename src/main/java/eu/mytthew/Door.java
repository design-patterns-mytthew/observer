package eu.mytthew;

public class Door {
    boolean status;
    DoorListener doorListener;
    String name;
    private boolean open = false;

    public void addListener(DoorListener doorListener) {
        this.doorListener = doorListener;
    }

    public Door(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean isOpen() {
        return open;
    }

    public void open() {
        open = true;
        doorListener.update(this, status);
        status = this.isOpen();
    }

    public void close() {
        open = false;
        doorListener.update(this, status);
        status = this.isOpen();
    }
}
