package eu.mytthew;

public interface DoorListener {
    void update(Door door, boolean status);
}
