package eu.mytthew;

import java.util.logging.Level;

public class Main {
	static DoorListener doorListener = (door, status) -> {
		if (door.isOpen() == status) {
			System.out.println("No change");
		}
		System.out.println(door.getName() + " status = " + door.isOpen());
	};

	public static void main(String[] args) {
		Door door = new Door("Vintage white door");
		door.addListener(doorListener);
		door.open();
		door.open();
		door.close();
		door.open();
		door.close();
		door.close();
	}
}
